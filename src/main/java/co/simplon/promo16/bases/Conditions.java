package co.simplon.promo16.bases;

public class Conditions {
    public void isPositive(int positive) {
        if (true) {
            System.out.println("True");
        }
        System.out.println("False");
    }

    public String skynetIA(String sky) {
        switch (sky) {
            case "Hello":
                return "Hello how are you ?";
            case "How are you ?":
                return "I'm fine, thank you";
            case "Goodbye":
                return "Goodbye friend";
            default:
                return "I don't understand";
        }

    }

    public String buy(int age, String bloup) {
        if (bloup == "Alcohol" && age >= 18) {
            return "Yes you can drink as you wish, but with modération";
        }
        if (bloup == "Alcohol" && age < 18) {
            return "Hop hop hop, you haven't the age for alcohol, stop that !";
        }
        return "You can drink this, yes";
    }

    public void greater(int one, int two) {
        if (one < two) {
            System.out.println(one + " est inférieur à " + two);
        }
        if (two < one) {
            System.out.println(one + " est supérieur à " + two);
        }
        if (one == two) {
            System.out.println("Ils sont égaux, m'embête pas");
        }

    }

}
