package co.simplon.promo16.bases;

import java.util.ArrayList;
import java.util.List;

public class Variables {

    public void first() {
        int age = 37;
        String namePromo = "P16";
        String loveJava = "C'est vrai, j'aime Java";
        List<String> listTechnKnow = new ArrayList<>(List.of(
               "Java", "React", "Javascript", "Node.js", "GabiLiam", "Symfony",
               "Vu.js", "Go", "Laravel", "Python", "C++", "Ruby", "PHP"));
        System.out.println(age);
        System.out.println(namePromo);
        System.out.println(loveJava);
        System.out.println(listTechnKnow);
    }

    public void withParameters(String string, int bloup) {
        System.out.println(string);
        System.out.println(bloup);
    }

    public String withReturn() {
        return "Le return";
    }
}
