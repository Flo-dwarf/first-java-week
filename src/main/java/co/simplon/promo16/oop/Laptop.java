package co.simplon.promo16.oop;

public class Laptop {
    String model;
    int battery;
    boolean turnedOn;
    boolean pluggedIn;
    String OS;

    public Laptop() {
        this.model = "";
        this.battery = 50;
        this.turnedOn = false;
        this.pluggedIn = false;
        this.OS = null;
    }

    public void plug() {
        pluggedIn = true;
        consumeBattery();
    }

    public void powerSwitch() {
        if (!turnedOn) {
            turnedOn = false;
            System.out.println(turnedOn);
            System.out.println(pluggedIn);
            System.out.println(battery);
        }
        if (turnedOn && !pluggedIn) {
            turnedOn = true;
            System.out.println(turnedOn);
            System.out.println(pluggedIn);
            System.out.println(battery);
        }
        if (turnedOn && pluggedIn && battery > 10) {
            turnedOn = true;
            consumeBattery();
            System.out.println(turnedOn);
            System.out.println(pluggedIn);
            System.out.println(battery);
        }
        return;
    }

    public void install(String OS) {
        if (!turnedOn) {
            if (pluggedIn) {
                consumeBattery();
            }
            OS = "L'OS a été installé !";
        }
    }

    private void consumeBattery() {
        if (pluggedIn) {
            battery -= 5;
        }
        if (!pluggedIn) {
            battery += 5;
        }
        if (battery == 0) {
            turnedOn = false;
        }
        if (battery >= 100) {
            battery = 100;
        }
    }
}
