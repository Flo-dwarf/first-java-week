package co.simplon.promo16;

import co.simplon.promo16.bases.Conditions;
import co.simplon.promo16.bases.Variables;
import co.simplon.promo16.oop.Laptop;

public class App {
    public static void main(String[] args) {
        // Variables variables = new Variables();
        // variables.first();
        // variables.withParameters("Bloup", 17);
        // System.out.println(variables.withReturn());

        // Conditions condition = new Conditions();
        // System.out.println(condition.skynetIA("Bloup"));
        // System.out.println(condition.buy(17, "Alcohol"));
        // condition.greater(17, 4);

        Laptop laptop = new Laptop();
        laptop.plug();
        laptop.powerSwitch();
     }

}
